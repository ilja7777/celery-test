from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response
from djoser.views import PasswordResetView as DjoserPasswordResetView
from djoser.conf import settings

User = get_user_model()
