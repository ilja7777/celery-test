from django.db import models
from django.contrib.auth.models import AbstractUser, AnonymousUser as AnonimousUserDefault
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.validators import ASCIIUsernameValidator, UnicodeUsernameValidator
from django.core.exceptions import ValidationError
import re
import sys

sys.path.append('./pycharm-debug-py3k.egg')
import pydevd
