from celery import Celery

#app = Celery('tasks', backend='amqp', broker='amqp://')
app = Celery('tasks', backend='amqp', broker='amqp://guest:**@localhost//')

@app.task
def print_hello(ignore_result=True):
    print('hello there')


@app.task
def gen_prime(x):
    multiples = []
    results = []
    for i in range(2, x+1):
        if i not in multiples:
            results.append(i)
            for j in range(i*i, x+1, i):
                multiples.append(j)
    return results